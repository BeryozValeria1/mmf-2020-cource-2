def Un(arr):
    dict = {}

    for i in range(0, len(arr)):
        if (arr[i] in dict):
            dict.update({arr[i]: -1})
        else:
            dict.update({arr[i]: i}) 

    ans = []

    for i in dict:
        if (dict[i] != -1):
            ans.append(arr[dict[i]])
    
    return ans
