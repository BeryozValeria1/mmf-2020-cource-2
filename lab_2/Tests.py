import Test
from solve import findNumber

Test.assert_equals(findNumber([2,2,2,1]),                1)
Test.assert_equals(findNumber([4,3,4,3,4,3,2]),          2)
print(findNumber([2020, 2020, 20, 20, 21]))
Test.assert_equals(findNumber([1,2,3,4,5,1,2,3,4]),      5)
Test.assert_equals(findNumber([5,4,2,4,5]),              2)
Test.assert_equals(findNumber([2,2,3,4,3,4,1]),          1)
Test.assert_equals(findNumber([111,111,2]),              111)
Test.assert_equals(findNumber([2020, 2020, 20, 20, 21]), 22)
Test.assert_equals(findNumber([323, 323, 42, 430, 430]), 323)
Test.assert_equals(findNumber([18, 19, 20, 18, 19]),     19)
Test.assert_equals(findNumber([1,2,3,4,5,1,2,3,4]),      1)